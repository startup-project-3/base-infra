variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"  # Set your desired default CIDR block
}

variable "region" {
  type    = string
  default = "us-east-1"  # Set your desired default CIDR block
}

provider "aws" {
  region = var.region
}

resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "MyVPC"
  }
}

output "vpc_id" {
  value = aws_vpc.my_vpc.id
}
